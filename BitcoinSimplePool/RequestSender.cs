﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Sockets;
using System.IO;
using Jayrock.Json;
using Jayrock.Json.Conversion;
using System.Threading;
using System.Net;
 
namespace BitcoinSimplePool
{
    /// <summary>
    /// Главный метод - Send. Он отправляет запрос (синхронный) партнеру и возвращает его ответ.
    /// В проекте используется для отправки только одного запроса - "getwork".
    /// 
    /// </summary>
    public class RequestSender
    {
        private int port;
        private int remotePort;
        private string remoteHost;
        private int mode;
        private TcpListener listener;
        private TcpClient client;
        private object clientLocker = new object();
        private Thread thread;
        private int id;
        private string path = "/";
        private bool isWeb = false;

        public const int CLIENT = 0;
        public const int SERVER = 1;

        public string Username { get; set; }
        public string Password { get; set; }

        public static RequestSender CreateServer(int port)
        {
            var ret = new RequestSender();
            ret.mode = SERVER;
            ret.port = port;
            return ret;
        }

        public static RequestSender CreateClient(string remoteHost, int remotePort, bool isWeb=false)
        {
            var ret = new RequestSender();
            ret.remoteHost = remoteHost;
            ret.remotePort = remotePort;
            ret.mode = CLIENT;
            ret.isWeb = isWeb;
            return ret;
        }

        /// <summary>
        /// Этот метод требуется вызвать перед началом работы с объектом.
        /// </summary>
        public void Start()
        {
            if (mode == SERVER)
            {
                thread = new Thread(thread_DoWork_Server);
                thread.IsBackground = true;
                thread.Start();
            }
        }

        /// <summary>
        /// В цикле проверяет состояние подключенного клиента, и в случае если он отвалился включает
        /// ожидание другого клиента.
        /// </summary>
        private void thread_DoWork_Server()
        {
            listener = new TcpListener(IPAddress.Any, port);
            listener.Start();
            while (true)
            {
                lock (clientLocker)
                {
                    if (client == null || !client.Connected)
                    {
                        client = listener.AcceptTcpClient();
                        Console.WriteLine("Connected receiver with " + client.Client.RemoteEndPoint);
                    }
                }
                Thread.Sleep(100);
            }
        }
        
        /// <summary>
        /// Оправляет запрос партнеру и возращает его твет.
        /// </summary>
        /// <returns>Ответ партнера.</returns>
        public object Send(string method, params object[] args)
        {
            if (mode == CLIENT)
            {
                if (isWeb)
                    return ExecClientWeb(remoteHost, remotePort, path, method, args);
                else
                    return ExecClient(remoteHost, remotePort, method, args);
            }
            else if (mode == SERVER)
                return ExecServer(method, args);
            throw new NotImplementedException();
        }

        private void WriteRequest(Stream stream, string method, params object[] args)
        {
            StreamWriter writer = new StreamWriter(stream, Encoding.UTF8);
            JsonObject call = new JsonObject();
            call["id"] = id++;
            call["method"] = method;
            call["params"] = args;
            StringWriter sw = new StringWriter();
            JsonConvert.Export(call, sw);
            writer.Write(sw.ToString());
            writer.Flush();
        }

        private string ReadResponse(Stream stream)
        {
            var response = "";
            var packet = "";
            try
            {
                StreamReader reader = new StreamReader(stream);
                do
                {
                    packet = reader.ReadLine();
                    response += packet;
                }
                while (packet != null);
            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR: {0}", ex.Message);
            }
            return response;
        }

        /// <summary>
        /// Отправляет запрос при работе в режиме "Сервера".
        /// </summary>
        /// <returns>
        /// Возврящает полученный от сервера результат. Если подключение не установлено,
        /// то возвращается null.
        /// </returns>
        private object ExecServer(string method, params object[] args)
        {
            lock (clientLocker)
            {
                if (this.client != null && this.client.Connected)
                {
                    WriteRequest(client.GetStream(), method, args);
                    return ReadResponse(client.GetStream());
                }
                else
                    return null;
            }
        }

        /// <summary>
        /// Устанавливает TCP-соединение с пулом и отправляет ему запрос.
        /// </summary>
        /// <returns>
        /// Возвращает полученный от сервера результат.
        /// </returns>
        private object ExecClient(string host, int port, string method, params object[] args)
        {
            using (TcpClient x = new TcpClient(host, port))
            {
                WriteRequest(x.GetStream(), method, args);
                return ReadResponse(x.GetStream());
            }
        }

        /// <summary>
        /// Устанавливает TCP-соединение с пулом и отправляет ему запрос.
        /// </summary>
        /// <returns>
        /// Возвращает полученный от сервера результат.
        /// </returns>
        private object ExecClientWeb(string host, int port, string path, string method, params object[] args)
        {
            Uri uri = new Uri(string.Format("http://{0}:{1}{2}", host, port, path));
            var request = WebRequest.Create(uri);
            request.Proxy = new WebProxy("192.168.60.3", 3128);
            request.Method = "POST";
            request.Headers.Add("Accept-Encoding", "deflate, gzip");
            request.ContentType = "application/json";
            request.Headers.Add("X-Mining-Extensions", "midstate");
            if (Username != null && Password != null)
                request.Credentials = new NetworkCredential(Username, Password);
            using (Stream stream = request.GetRequestStream())
            {
                WriteRequest(stream, method, args);
            }
            using (Stream stream = request.GetResponse().GetResponseStream())
                return ReadResponse(stream);
        }
    }
}
