﻿using System;
using System.Collections.Generic;
using System.Text;
using Jayrock.JsonRpc;

namespace BitcoinSimplePool
{
    /// <summary>
    /// Представляет API биткойн-пула.
    /// </summary>
    public class BitcoinPoolService : JsonRpcService
    {
        public BitcoinPoolService(RequestSender sender)
        {
            Sender = sender;
        }

        public RequestSender Sender
        {
            get;
            private set;
        }

        [JsonRpcMethod("test")]
        public int Test(int a, int b)
        {
            return a + b;
        }

        [JsonRpcMethod("getwork")]
        public object GetWork(string data)
        {
            return Sender.Send("getwork", data);
        }
    }

    public class BitcoinPoolServiceCreator : ServiceCreator
    {
        public BitcoinPoolServiceCreator(RequestSender sender)
        {
            Sender = sender;
        }

        public RequestSender Sender
        {
            get;
            private set;
        }

        public JsonRpcService Create()
        {
            return new BitcoinPoolService(Sender);
        }
    }
}
