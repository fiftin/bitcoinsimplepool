﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Sockets;
using System.Net;
using System.IO;
using Jayrock.JsonRpc;

namespace BitcoinSimplePool
{
    /// <summary>
    /// Представляет пул к которому цепляются майнеры. Он передает из запросы на другой пул.
    /// </summary>
    public class Pool
    {
        /// <summary>
        /// Занимается обработкой запросов от майнеров.
        /// </summary>
        private RequestProcessor processor;

        /// <summary>
        /// Отправляет полученные от майнеров запросы на пул-родитель.
        /// </summary>
        private RequestSender sender;

        /// <summary>
        /// Creates the server.
        /// </summary>
        public static Pool CreateServer(int portForReplicator, int portForClients, bool isWeb)
        {
            Pool ret = new Pool();
            ret.sender = RequestSender.CreateServer(portForReplicator);
            ret.processor = RequestProcessor.CreateSever(portForClients, new BitcoinPoolServiceCreator(ret.sender), isWeb);
            return ret;
        }

        /// <summary>
        /// Creates the client.
        /// </summary>
        public static Pool CreateClient(string replicatorHost, int replicatorPort, string host, int port, bool isWeb)
        {
            Pool ret = new Pool();
            ret.sender = RequestSender.CreateClient(host, port, isWeb);
            ret.processor = RequestProcessor.CreateClient(replicatorHost, replicatorPort, new BitcoinPoolServiceCreator(ret.sender));
            return ret;
        }

        public RequestProcessor Processor
        {
            get
            {
                return processor;
            }
        }

        public RequestSender Sender
        {
            get
            {
                return sender;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Username
        {
            get
            {
                return sender.Username;
            }
            set
            {
                sender.Username = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Password
        {
            get
            {
                return sender.Password;
            }
            set
            {
                sender.Password = value;
            }
        }

        /// <summary>
        /// Начинает принимать запросы.
        /// </summary>
        public void Start()
        {
            processor.Start();
            sender.Start();
        }
    }
}
