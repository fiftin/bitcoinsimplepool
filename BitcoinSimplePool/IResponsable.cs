﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace rmortega77.CsHTTPServer
{
    public interface IResponsable
    {
        void OnResponse(ref HTTPRequestStruct rq, ref HTTPResponseStruct rp);
        void WriteLog(string EventMessage);
    }
}
    