﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Sockets;
using System.IO;
using Jayrock.JsonRpc;
using System.Threading;
using System.Net;
using rmortega77.CsHTTPServer;

namespace BitcoinSimplePool
{
    public interface ServiceCreator
    {
        JsonRpcService Create();
    }

    /// <summary>
    /// При вызове метода Start, создается новый поток который постоянно считывает
    /// запросы от партнера и перенапаравляет их объекту Sender.
    /// </summary>
    public class RequestProcessor : IResponsable
    {

        private int mode;

        private TcpListener requestListener;
        private int port;

        private TcpClient client;
        private string remoteHost;
        private int remotePort;

        private Thread thread;

        private ServiceCreator serviceCreator;

        private bool isWeb;

        public const int CLIENT = 0;
        public const int SERVER = 1;

        public static RequestProcessor CreateSever(int port, ServiceCreator creator, bool isWeb=false)
        {
            var ret = new RequestProcessor();
            ret.port = port;
            ret.mode = SERVER;
            ret.serviceCreator = creator;
            ret.isWeb = isWeb;
            return ret;
        }

        public static RequestProcessor CreateClient(string remoteHost, int remotePort, ServiceCreator creator, bool isWeb=false)
        {
            var ret = new RequestProcessor();
            ret.remoteHost = remoteHost;
            ret.remotePort = remotePort;
            ret.mode = CLIENT;
            ret.serviceCreator = creator;
            ret.isWeb = isWeb;
            return ret;
        }

        public void Start()
        {
            switch (mode)
            {
                case SERVER:
                    thread = new Thread(thread_DoWork_Server);
                    break;
                case CLIENT:
                    thread = new Thread(thread_DoWork_Client);
                    break;
                default:
                    throw new Exception("Invalid mode");
            }
            thread.IsBackground = true;
            thread.Start();
        }

        /// <summary>
        /// Образатывает запросы в режиме КЛИЕНТ.
        /// </summary>
        private void thread_DoWork_Client()
        {
            while (true)
            {
                try
                {
                    if (client == null || !client.Connected)
                    {
                        client = new TcpClient(remoteHost, remotePort);
                        Console.WriteLine("Connected to server {0}:{1}", remoteHost, remotePort);
                    }
                    else
                    {
                        Process(client);
                    }
                } catch (Exception ex)
                {
                    Console.WriteLine("ERROR: " + ex.Message);
                }
                Thread.Sleep(100);
            }
        }

        /// <summary>
        /// Обрабатывает запросы в режиме СЕРВЕР.
        /// </summary>
        private void thread_DoWork_Server()
        {
            requestListener = new TcpListener(System.Net.IPAddress.Any, port);
            requestListener.Start();
            while (true)
            {
                try
                {
                    using (var x = requestListener.AcceptTcpClient())
                    {
                        Console.WriteLine("Connected sender with " + x.Client.RemoteEndPoint);
                        using (x)
                        {
                            Process(x);
                        }
                    }
                } catch (Exception ex)
                {
                    Console.WriteLine("ERROR: " + ex.Message);
                }
                Thread.Sleep(100);
            }
        }

        public void Process(TcpClient client)
        {
            if (isWeb)
            {
                CsHTTPRequest request = new CsHTTPRequest(client, this);
                request.Process();
            }
            else
            {
                var reader = new StreamReader(client.GetStream(), Encoding.UTF8);
                var writer = new StreamWriter(client.GetStream(), new UTF8Encoding(false));
                var dispatcher = JsonRpcDispatcherFactory.CreateDispatcher(serviceCreator.Create());
                dispatcher.Process(reader, writer);
                writer.Flush();
            }
        }

        public void OnResponse(ref HTTPRequestStruct rq, ref HTTPResponseStruct rp)
        {
            StringBuilder sb = new StringBuilder();
            var reader = new StringReader(Encoding.ASCII.GetString(rq.BodyData));
            var writer = new StringWriter(sb);
            var dispatcher = JsonRpcDispatcherFactory.CreateDispatcher(serviceCreator.Create());
            dispatcher.Process(reader, writer);
            writer.Flush();
            rp.Headers.Add("Content-Type", "application/json");
            rp.BodyData = Encoding.ASCII.GetBytes(sb.ToString());
            rp.BodySize = rp.BodyData.Length;
        }

        public void WriteLog(string EventMessage)
        {
            Console.WriteLine(EventMessage);
        }
    }
}
