﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using Jayrock.JsonRpc;

namespace BitcoinSimplePool
{
    static class Program
    {
        static void Main(String[] args)
        {
            var mode = "server";
            var host = "localhost";
            var port = 13000;
            var rHost = "localhost";
            var rPort = 10000;
            var isWeb = false;
            string username = null;
            string password = null;
            for (int i = 0; i < args.Length; i++)
            {
                try
                {
                    switch (args[i])
                    {
                        case "-m": // mode: client, server
                            mode = args[++i];
                            break;
                        case "-h": // host
                            host = args[++i];
                            break;
                        case "-p": // port
                            port = int.Parse(args[i + 1]);
                            i++;
                            break;
                        case "--rhost": // replicator host
                            rHost = args[++i];
                            break;
                        case "--rport": // replicator port
                            rPort = int.Parse(args[i + 1]);
                            i++;
                            break;
                        case "--web":
                            isWeb = true;
                            break;
                        case "--pass": // password
                            password = args[i + 1];
                            break;
                        case "-u": // username
                            username = args[i + 1];
                            break;
                    }
                } catch (Exception)
                {
                    Console.WriteLine("ERROR: Invalid command line argument ({0}).", i);
                }
            }

            Pool pool;
            if (mode == "server")
                pool = Pool.CreateServer(rPort, port, isWeb);
            else if (mode == "client")
                pool = Pool.CreateClient(rHost, rPort, host, port, isWeb);
            else
            {
                Console.WriteLine("ERROR: Invalid mode ({0}). Must be 'server' or 'client'.", mode); 
                return;
            }
            pool.Password = password;
            pool.Username = username;

            Console.WriteLine("Mode: {0}", mode);
            Console.WriteLine("Host: {0}", host);
            Console.WriteLine("Port: {0}", port);
            Console.WriteLine("Replicator Host: {0}", rHost);
            Console.WriteLine("Replicator Port: {0}", rPort);
            Console.WriteLine("Is Web: {0}", isWeb);
            if (username != null)
                Console.WriteLine("Username: {0}", username);
            Console.ForegroundColor = ConsoleColor.DarkYellow;
            if (mode == "server")
                Console.WriteLine("SERVER started");
            else if (mode == "client")
                Console.WriteLine("CLIENT started");
            Console.ResetColor();
            pool.Start();
            Console.ReadLine();
        }
    }
}
