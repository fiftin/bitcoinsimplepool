﻿using System;
using System.Collections.Generic;
using System.Text;
using Jayrock.Json;
using Jayrock.JsonRpc;
using System.Net.Sockets;
using System.IO;
using System.Diagnostics;
using Jayrock.Json.Conversion;
using System.Collections;
using System.Net;

namespace TestClient
{
    class Program
    {
        static int id = 0;
        static object Exec(string host, int port, string method, params object[] args)
        {
            string response = "NULL";
            TcpClient socket = new TcpClient();
            socket.Connect(host, port);
            NetworkStream ns = socket.GetStream();
            StreamWriter sw = new StreamWriter(ns);
            JsonObject call = new JsonObject();
            call["id"] = ++id;
            call["method"] = method;
            call["params"] = args;
            JsonConvert.Export(call, sw);
            sw.Flush();
            string packet = string.Empty;
            StreamReader sr = new StreamReader(ns);
            do
            {
                packet = sr.ReadLine();
                Console.WriteLine(":"+packet+"'");
                response += packet;
            }
            while (packet != null);
            socket.Close();
            return (response);
        }

        static void Main(string[] args)
        {
            var y = Exec("127.0.0.1", 13000, "getwork", new object[] { "" });
            Console.WriteLine(y.GetType().Name);
            Console.WriteLine(y);
        }

        static object OnResponse(JsonReader reader, Type returnType)
        {
            Debug.Assert(reader != null);
            Debug.Assert(returnType != null);

            NamedJsonBuffer[] members = JsonBuffer.From(reader).GetMembersArray();
            foreach (NamedJsonBuffer member in members)
            {
                if (string.CompareOrdinal(member.Name, "error") == 0)
                {
                    object errorObject = JsonConvert.Import(member.Buffer.CreateReader());
                    if (errorObject != null)
                        OnError(errorObject);
                }
                else if (string.CompareOrdinal(member.Name, "result") == 0)
                {
                    return returnType != typeof(JsonBuffer)
                         ? JsonConvert.Import(returnType, member.Buffer.CreateReader())
                         : member.Buffer;
                }
            }

            throw new Exception("Invalid JSON-RPC response. It contains neither a result nor an error.");
        }

        static void OnError(object errorObject)
        {
            IDictionary error = errorObject as IDictionary;

            if (error != null)
                throw new Exception(error["message"] as string);

            throw new Exception(errorObject as string);
        }



    }
}
