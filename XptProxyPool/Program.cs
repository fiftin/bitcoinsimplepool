﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;

namespace XptProxyPool
{
    class Program
    {


        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        static void HandleCommand(string command, ProxyPool pool)
        {
            switch (command)
            {
                case "die":
                    pool.SendCommand(ProxyPool.COMMAND_DIE);
                    break;
                case "reconnect":
                    pool.SendCommand(ProxyPool.COMMAND_RECONNECT);
                    break;
                case "pause":
                    pool.SendCommand(ProxyPool.COMMAND_PAUSE);
                    break;
                case "resume":
                    pool.SendCommand(ProxyPool.COMMAND_RESUME);
                    break;
                default:
                    if (command.StartsWith("threads "))
                    {
                        uint noThreads = uint.Parse(command.Substring("threads ".Length).Trim());
                        pool.SendCommand(ProxyPool.COMMAND_THREADS, noThreads);
                    }
                    break;
            }
        }

        static void Main(string[] args)
        {
            int port = 1034;
            int serverPort = 8083;
            string serverHost = "ypool.net";
            string proxyHost = "";
            int proxyPort = 0;
            for (int i = 0; i < args.Length; i++)
            {
                try
                {
                    switch (args[i])
                    {
                        case "-p":
                            port = int.Parse(args[++i]);
                            break;
                        case "-s":
                            serverPort = int.Parse(args[++i]);
                            break;
                        case "-h":
                            serverHost = args[++i];
                            break;
                        case "--proxy-host":
                            proxyHost = args[++i];
                            break;
                        case "--proxy-port":
                            proxyPort = int.Parse(args[++i]);
                            break;
                    }
                }
                catch (Exception)
                {
                    Log.Write("Invalid command line argument ({0}).", i, LogType.FatalError, LogLocation.None);
                    return;
                }
            }

            ProxyPool pool = new ProxyPool(port, serverHost, serverPort, proxyHost, proxyPort);
            pool.Start();
            Console.WriteLine("Server     : {0}:{1}", serverHost, serverPort);
            if (!string.IsNullOrWhiteSpace(proxyHost))
                Console.WriteLine("Proxy      : {0}:{1}", proxyHost, proxyPort);
            Console.WriteLine("Client Port: {0}", port);
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("You can enter commands: die, reconnect, pause, resume, exit, thrads <n>");
            Console.ResetColor();
            var command = Console.ReadLine().ToLower();
            while (command != "exit")
            {
                try
                {
                    HandleCommand(command, pool);
                }
                catch (Exception ex)
                {
                    Log.Write(ex.Message, LogType.Error, LogLocation.None);
                }
                command = Console.ReadLine().ToLower();
            }
            Log.Write("Exiting...");
            System.Threading.Thread.Sleep(1000);
        }
    }
}
