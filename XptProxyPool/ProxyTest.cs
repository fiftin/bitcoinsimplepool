﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Starksoft.Net.Proxy;
using System.Threading;

namespace XptProxyPool
{
    public class ProxyTest
    {
        public readonly HttpProxyClient Proxy = new HttpProxyClient("192.168.60.3", 3128);
        public string ReadContent()
        {
            var client = Proxy.CreateConnection("ypool.net", 8083);
            var stream = client.GetStream();
            var req = new ProxyPool.Res();
            req.header = new ProxyPool.Header();
            req.header.opcode = ProxyPool.XPT_OPC_C_AUTH_REQ;
            Thread.Sleep(3000);
            System.IO.MemoryStream dataStream = new System.IO.MemoryStream();
            ProxyPool.Write32((uint)5, dataStream);
            ProxyPool.WriteString("fiftin.pts_1", dataStream);
            ProxyPool.WriteString("123", dataStream);
            ProxyPool.Write32((uint)10, dataStream);
            ProxyPool.WriteString("jhProtominer v0.1c", dataStream);
            dataStream.Flush();
            req.header.len = (uint)dataStream.Length;
            req.data = new byte[req.header.len];
            Buffer.BlockCopy(dataStream.GetBuffer(), 0, req.data, 0, req.data.Length);
            ProxyPool.WriteRes(req, stream);

            Thread.Sleep(1000);
            int av = client.Client.Available;
            byte[] bytes = new byte[av];
            var n = stream.Read(bytes, 0, bytes.Length);
            return "ok";

        }
    }
}
