﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Threading;
using System.Net;
using System.IO;
using System.Runtime.InteropServices;
using Starksoft.Net.Proxy;

namespace XptProxyPool
{
    public class ProxyPool
    {
        public const int XPT_OPC_C_AUTH_REQ = 1;
        public const int XPT_OPC_S_AUTH_ACK = 2;
        public const int XPT_OPC_S_WORKDATA1 = 3;

        public const int XPT_OPC_C_SUBMIT_SHARE = 4;
        public const int XPT_OPC_S_SHARE_ACK = 5;

        public const int XPT_OPC_C_SUBMIT_POW = 6;
        public const int XPT_OPC_S_MESSAGE = 7;

        public const int XPT_EXT_S_COMMAND = 33;

        public const uint COMMAND_DIE = 1;
        public const uint COMMAND_RECONNECT = 2;
        public const uint COMMAND_PAUSE = 3;
        public const uint COMMAND_RESUME = 4;
        public const uint COMMAND_THREADS = 5;

        public string GetCommandName(uint command)
        {
            switch (command)
            {
                case COMMAND_DIE:
                    return "DIE";
                case COMMAND_RECONNECT:
                    return "RECONNECT";
                case COMMAND_PAUSE:
                    return "PAUSE";
                case COMMAND_RESUME:
                    return "RESUME";
                case COMMAND_THREADS:
                    return "THREADS";
                default:
                    return "UNKNOWN";
            }
        }

        public static string GetOpcodeName(int opcode)
        {
            switch (opcode)
            {
                case XPT_OPC_C_AUTH_REQ:
                    return "AUTH_REQ";
                case XPT_OPC_S_AUTH_ACK:
                    return "AUTH_ACK";
                case XPT_OPC_S_WORKDATA1:
                    return "WORKDATA1";
                case XPT_OPC_C_SUBMIT_SHARE:
                    return "SUBMIT_SHARE";
                case XPT_OPC_S_SHARE_ACK:
                    return "SHARE_ACK";
                case XPT_OPC_C_SUBMIT_POW:
                    return "SUBMIT_POW";
                case XPT_OPC_S_MESSAGE:
                    return "MESSAGE";
                case XPT_EXT_S_COMMAND:
                    return "COMMAND";
                default:
                    return "UNKNOWN";
            }
        }

        public class Header
        {
            public byte opcode;
            public uint len;
        }

        public class Res
        {
            public Header header;
            public byte[] data;
        }

        int port;
        int serverPort;
        string serverHost;

        object clientLocker = new object();

        TcpClient client;
        TcpClient server;
        Thread clientListenThread;
        Thread requestReceiveThread;
        Thread responseReceiveThread;
        Thread processThread;


        Queue<Res> requests = new Queue<Res>();
        Queue<Res> responses = new Queue<Res>();
        NetworkStream serverStream;
        NetworkStream clientStream;

        bool running = false;

        IProxyClient proxy;

        public ProxyPool(int port, string serverHost, int serverPort, string proxyHost, int proxyPort)
        {
            this.port = port;
            this.serverHost = serverHost;
            this.serverPort = serverPort;
            if (string.IsNullOrWhiteSpace(proxyHost))
                proxy = new XptProxyPool.Proxy.NoProxyClient();
            else
                proxy = new HttpProxyClient(proxyHost, proxyPort);
        }

        public void Start()
        {
            if (clientListenThread != null && (clientListenThread.ThreadState & ThreadState.Running) == ThreadState.Running)
                throw new Exception("Pool already started");
            clientListenThread = new Thread(clientListenThread_DoWork);
            clientListenThread.Name = "clientListenThread";
            clientListenThread.IsBackground = true;
            clientListenThread.Start();
        }


        public static uint Read24(Stream stream)
        {
            byte[] bytes = new byte[4];
            stream.Read(bytes, 0, 3);
            return ByteArrayToStructure<uint>(bytes);
        }

        public static uint Read32(Stream stream)
        {
            byte[] bytes = new byte[4];
            stream.Read(bytes, 0, bytes.Length);
            return ByteArrayToStructure<uint>(bytes);
        }

        public static byte[] ReadString(Stream stream)
        {
            int len = stream.ReadByte();
            byte[] bytes = new byte[len];
            stream.Read(bytes, 0, len);
            return bytes;
        }

        public static byte[] ReadLongString(Stream stream)
        {
            byte[] lenBytes = new byte[2];
            stream.Read(lenBytes, 0, lenBytes.Length);
            ushort len = ByteArrayToStructure<ushort>(lenBytes);
            byte[] bytes = new byte[len];
            stream.Read(bytes, 0, len);
            return bytes;
        }

        public static Header ReadHeader(Stream stream)
        {
            int firstByte = stream.ReadByte();
            if (firstByte < 0)
                throw new Exception("-1");
            return new Header()
            {
                opcode = (byte)firstByte,
                len = Read24(stream)
            };
        }

        public static byte[] GetHeaderBytes(Header header)
        {
            byte[] bytes = new byte[4];
            bytes[0] = header.opcode;
            Buffer.BlockCopy(StructureToByteArray(header.len), 0, bytes, 1, 3);
            return bytes;
        }

        public static void Write32(uint i, Stream stream)
        {
            stream.Write(StructureToByteArray(i), 0, 4);
        }

        public static void Write24(uint i, Stream stream)
        {
            stream.Write(StructureToByteArray(i), 0, 3);
        }

        public static int WriteString(string i, Stream stream)
        {
            return WriteString(Encoding.UTF8.GetBytes(i), stream);
        }

        public static int WriteString(byte[] i, Stream stream)
        {
            if (i.Length > 255)
                throw new Exception("len(i) > 255");
            byte[] buf = new byte[i.Length + 1];
            buf[0] = (byte)i.Length;
            Buffer.BlockCopy(i, 0, buf, 1, i.Length);
            stream.Write(buf, 0, buf.Length);
            return i.Length;
        }

        public static int WriteLongString(byte[] i, Stream stream)
        {
            if (i.Length > 65535)
                throw new Exception("len(i) > 65535");

            var lenBytes = StructureToByteArray((ushort)i.Length);
            byte[] buf = new byte[i.Length + 2];
            Buffer.BlockCopy(lenBytes, 0, buf, 0, lenBytes.Length);
            Buffer.BlockCopy(i, 0, buf, 2, i.Length);
            stream.Write(buf, 0, buf.Length);
            return i.Length;
        }

        public static int WriteLongString(string i, Stream stream)
        {
            return WriteLongString(Encoding.ASCII.GetBytes(i), stream);
        }


        public static Res ReadRes(Stream stream)
        {
            var ret = new Res();
            ret.header = ReadHeader(stream);
            if (ret.header.opcode < XPT_OPC_C_AUTH_REQ || ret.header.opcode > XPT_OPC_S_MESSAGE)
                throw new Exception("Invalid opcode " + ret.header.opcode);
            if (ret.header.len == 0)
                throw new Exception("Invalid len " + ret.header.len);

            ret.data = new byte[ret.header.len];
            stream.Read(ret.data, 0, ret.data.Length);
            return ret;
        }

        public static void WriteRes(Res req, Stream stream)
        {
            byte[] headerBytes = GetHeaderBytes(req.header);
            byte[] bytes = new byte[headerBytes.Length + req.data.Length];
            Buffer.BlockCopy(headerBytes, 0, bytes, 0, headerBytes.Length);
            Buffer.BlockCopy(req.data, 0, bytes, headerBytes.Length, req.data.Length);
            stream.Write(bytes, 0, bytes.Length);
            stream.Flush();
        }

        public void processThread_DoWork()
        {
            while (running)
            {
                try
                {
                    lock (requests)
                    {
                        while (requests.Count > 0)
                        {
                            var req = requests.Dequeue();
                            WriteRes(req, serverStream);
                            // Request sent.
                            Log.Write("{0} ({1})", GetOpcodeName(req.header.opcode), req.header.opcode,
                                LogType.Info, 
                                LogLocation.ReqProcessing);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log.Write(ex.Message, LogType.FatalError, LogLocation.ReqProcessing);
                    running = false;
                    break;
                }

                Thread.Sleep(1000);
                try
                {
                    lock (responses)
                    {
                        while (responses.Count > 0)
                        {
                            var resp = responses.Dequeue();
                            WriteRes(resp, clientStream);
                            // Response sent. 
                            Log.Write("{0} ({1})", GetOpcodeName(resp.header.opcode), resp.header.opcode,
                                LogType.Info, 
                                LogLocation.ResponseProcessing);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log.Write(ex.Message, LogType.FatalError, LogLocation.ResponseProcessing);
                    running = false;
                    break;
                }
                Thread.Sleep(1000);
            }
            try
            {
                SendCommand(COMMAND_RECONNECT);
                client.Close();
                server.Close();
                serverStream = null;
                clientStream = null;
            }
            catch (Exception ex)
            {
                Log.Write(ex.Message, LogType.Error, LogLocation.None);
            }
            Log.Write("Waiting new connection...");
        }

        private void responseReceiveThread_DoWork()
        {
            try
            {
                while (running)
                {
                    var resp = ReadRes(serverStream);
                    lock (responses)
                    {
                        responses.Enqueue(resp);
                        // Response received.
                        Log.Write("{0} ({1})", GetOpcodeName(resp.header.opcode), resp.header.opcode, 
                            LogType.Info,
                            LogLocation.ResponseIn);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex.Message, LogType.FatalError, LogLocation.ResponseIn);
                running = false;
            }
        }

        private void requestReceiveThread_DoWork()
        {
            try
            {
                while (running)
                {
                    var req = ReadRes(clientStream);
                    lock (requests)
                    {
                        requests.Enqueue(req);
                        // Req received.
                        Log.Write("{0} ({1})", GetOpcodeName(req.header.opcode), req.header.opcode, LogType.Info, LogLocation.ReqIn);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex.Message, LogType.FatalError, LogLocation.ReqIn);
                running = false;
            }
        }

        private void clientListenThread_DoWork()
        {
            TcpListener listener = new TcpListener(IPAddress.Any, port);
            listener.Start();

            Log.Write("Waiting new connection...");
            while (true)
            {
                try
                {
                    var newClient = listener.AcceptTcpClient();

                    // прекращаем работу предыдущего клиента
                    running = false;
                    Thread.Sleep(1000);
                    if (client != null && client.Connected)
                        client.Close();
                    if (server != null && server.Connected)
                        server.Close();
                    if (requestReceiveThread != null && (requestReceiveThread.ThreadState & ThreadState.Running) == ThreadState.Running)
                        requestReceiveThread.Abort();
                    if (responseReceiveThread != null && (responseReceiveThread.ThreadState & ThreadState.Running) == ThreadState.Running)
                        responseReceiveThread.Abort();
                    if (processThread != null && (processThread.ThreadState & ThreadState.Running) == ThreadState.Running)
                        processThread.Abort();


                    // запускаем нового клиента

                    client = newClient;

                    Log.Write("Client connected. IP " + client.Client.RemoteEndPoint);
                    clientStream = client.GetStream();

                    server = proxy.CreateConnection(serverHost, serverPort);
                    Log.Write("Connected to server. IP " + server.Client.RemoteEndPoint);
                    serverStream = server.GetStream();

                    running = true;

                    requestReceiveThread = new Thread(requestReceiveThread_DoWork);
                    requestReceiveThread.Name = "requestReceiveThread";
                    requestReceiveThread.IsBackground = true;

                    responseReceiveThread = new Thread(responseReceiveThread_DoWork);
                    responseReceiveThread.Name = "responseReceiveThread";
                    responseReceiveThread.IsBackground = true;

                    processThread = new Thread(processThread_DoWork);
                    processThread.Name = "processThread";
                    processThread.IsBackground = true;

                    requestReceiveThread.Start();
                    responseReceiveThread.Start();
                    processThread.Start();
                }
                catch (Exception ex)
                {
                    Log.Write(ex.Message, LogType.Error, LogLocation.None);
                    Thread.Sleep(10000);
                }
            }
        }

        public void SendCommand<T>(uint command, T value) where T : struct
        {
            SendCommand(command, StructureToByteArray<T>(value));
        }

        public void SendCommand(uint command, byte[] data)
        {
            byte[] commandBytes = StructureToByteArray(command);
            byte[] bytes = new byte[commandBytes.Length + data.Length];
            Buffer.BlockCopy(commandBytes, 0, bytes, 0, commandBytes.Length);
            Buffer.BlockCopy(data, 0, bytes, commandBytes.Length, data.Length);
            var res = new Res();
            res.header = new Header();
            res.header.opcode = XPT_EXT_S_COMMAND;
            res.header.len = (uint)bytes.Length;
            res.data = bytes;
            WriteRes(res, clientStream);
        }

        public void SendCommand(uint command)
        {
            SendCommand(command, new byte[0]);
        }

        public static T ByteArrayToStructure<T>(byte[] bytes) where T : struct
        {
            GCHandle handle = GCHandle.Alloc(bytes, GCHandleType.Pinned);
            T stuff = new T();
            object obj = Marshal.PtrToStructure(handle.AddrOfPinnedObject(), typeof(T));
            stuff = (T)obj;
            handle.Free();
            return stuff;
        }

        public static byte[] StructureToByteArray<T>(T structure) where T : struct
        {
            int headerSize = Marshal.SizeOf(typeof(T));
            IntPtr p = Marshal.AllocHGlobal(headerSize);
            Marshal.StructureToPtr(structure, p, false);
            byte[] bytes = new byte[headerSize];
            Marshal.Copy(p, bytes, 0, headerSize);
            Marshal.FreeHGlobal(p);
            return bytes;
        }
    }
}
