﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XptProxyPool
{
    enum LogType
    {
        FatalError,
        Error,
        Info
    }

    enum LogLocation
    {
        None,
        ReqProcessing,
        ResponseProcessing,
        ReqIn,
        ResponseIn,
    }

    static class Log
    {
        public static void Write(string s)
        {
            Write(s, LogType.Info, LogLocation.None);
        }

        public static void Write(string s, LogType type, LogLocation loc)
        {
            string prefix;
            switch (type)
            {
                case LogType.FatalError:
                    prefix = "FATAL ";
                    break;
                case LogType.Error:
                    prefix = "ERR ";
                    break;
                case LogType.Info:
                default:
                    prefix = "";
                    break;
            }

            switch (loc)
            {
                case LogLocation.ReqIn:
                    prefix = " IN QUERY     " + prefix;
                    break;
                case LogLocation.ReqProcessing:
                    prefix = " OUT QUERY    " + prefix;
                    break;
                case LogLocation.ResponseIn:
                    prefix = " IN RESPONSE  " + prefix;
                    break;
                case LogLocation.ResponseProcessing:
                    prefix = " OUT RESPONSE " + prefix;
                    break;
                default:
                    prefix = "              " + prefix;
                    break;
            }
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.Write(DateTime.Now);
            switch (type)
            {
                case LogType.Info:
                    Console.ForegroundColor = ConsoleColor.Gray;
                    break;
                case LogType.Error:
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    break;
                case LogType.FatalError:
                    Console.ForegroundColor = ConsoleColor.Red;
                    break;
            }
            Console.Write(prefix);
            Console.ResetColor();
            Console.Write(s);
            Console.WriteLine();
        }

        public static void Write(string format, object arg1, LogType type, LogLocation loc)
        {
            Write(string.Format(format, arg1), type, loc);
        }

        public static void Write(string format, object arg1, object arg2, LogType type, LogLocation loc)
        {
            Write(string.Format(format, arg1, arg2), type, loc);
        }
    }
}
