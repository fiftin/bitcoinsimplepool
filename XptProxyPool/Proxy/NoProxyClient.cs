﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Starksoft.Net.Proxy;
using System.Net.Sockets;

namespace XptProxyPool.Proxy
{
    public class NoProxyClient : IProxyClient
    {
        public event EventHandler<CreateConnectionAsyncCompletedEventArgs> CreateConnectionAsyncCompleted;

        public string ProxyHost
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public int ProxyPort
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string ProxyName
        {
            get { throw new NotImplementedException(); }
        }

        public System.Net.Sockets.TcpClient TcpClient
        {
            get
            {
                return client;
            }
            set
            {
                client = value;
            }
        }

        private TcpClient client;

        public System.Net.Sockets.TcpClient CreateConnection(string destinationHost, int destinationPort)
        {
            client = new TcpClient(destinationHost, destinationPort);
            return client;
        }

        public void CreateConnectionAsync(string destinationHost, int destinationPort)
        {
            throw new NotImplementedException();
        }
    }
}
